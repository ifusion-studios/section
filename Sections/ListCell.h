//
//  ListCell.h
//  Sections
//
//  Created by Samir Kha on 24/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell{
    
}

@property (retain, nonatomic) IBOutlet UILabel *cafeName;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@end
