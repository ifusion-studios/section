//
//  BIDMasterViewController.h
//  Sections
//
//  Created by Samir Kha on 24/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSDictionary+MutableDeepCopy.h"
#import "BIDAppDelegate.h"
#import "ListCell.h"
@class BIDDetailViewController;

@interface BIDMasterViewController : UITableViewController
<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    BIDAppDelegate *appDel;
}

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UISearchBar *search;
@property (strong, nonatomic) NSDictionary *allNames;
@property (strong, nonatomic) NSMutableDictionary *names;
@property (strong, nonatomic) NSMutableArray *keys;
@property (assign, nonatomic) BOOL isSearching;
- (void)resetSearch;
- (void)handleSearchForTerm:(NSString *)searchTerm;
@property (strong, nonatomic) BIDDetailViewController *detailViewController;

@end
