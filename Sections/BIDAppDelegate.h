//
//  BIDAppDelegate.h
//  Sections
//
//  Created by Samir Kha on 24/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDAppDelegate : UIResponder <UIApplicationDelegate>{
     NSString *_fileName;
}
@property(nonatomic, retain, readwrite)NSString *fileName;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
