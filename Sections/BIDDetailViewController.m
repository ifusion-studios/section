//
//  BIDDetailViewController.m
//  Sections
//
//  Created by Samir Kha on 24/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BIDDetailViewController.h"
@implementation BIDDetailViewController



@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDel=[[UIApplication sharedApplication]delegate];
    self.title=appDel.fileName;
    // self.navigationItem.title=appDel.fileName;
    
    NSString*fName=[NSString stringWithFormat:@"%@",appDel.fileName];
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:fName ofType:@"htm" inDirectory:nil];
    
    NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
    [webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:[NSURL URLWithString:@""]];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    
    [self setWebView:nil];
   
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

				
@end
